#!/bin/sh
#
# SatNOGS client setup script
#
# Copyright (C) 2017 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PREFIX="/usr/local/share/satnogs-setup"
BOOTSTRAP_SCRIPT="bootstrap.sh"
SETUP_SCRIPT="setup.sh"
CONFIG_SCRIPT="config.sh"

if [ -x "${PREFIX}/${BOOTSTRAP_SCRIPT}" ]; then
	. "${PREFIX}/${BOOTSTRAP_SCRIPT}"
fi

if [ -x "${PREFIX}/${CONFIG_SCRIPT}" ]; then
	. "${PREFIX}/${CONFIG_SCRIPT}"
fi

if [ -x "${PREFIX}/${SETUP_SCRIPT}" ]; then
	. "${PREFIX}/${SETUP_SCRIPT}"
fi
